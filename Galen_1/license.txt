﻿The FontStruction “Galen_1”
(https://fontstruct.com/fontstructions/show/2316111) by English Parsig is
licensed under a Creative Commons Attribution Share Alike license
(http://creativecommons.org/licenses/by-sa/3.0/).
“Galen_1” was originally cloned (copied) from the FontStruction
“Struktur” (https://fontstruct.com/fontstructions/show/175) by Ivan Bettger,
which is licensed under a Creative Commons Attribution Share Alike license
(http://creativecommons.org/licenses/by-sa/3.0/).