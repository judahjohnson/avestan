# Avestan 
Following list shows Avestan corresponding English letters and Unicode characters which could be used to write Sara Pârsig (refined Persian).<br/> 
Also, north Indian notation systems have been adopted for digits.<br/>
Digits and punctuation marks have been flipped horizontally, as Avestan is written from right to left.<br/>
There are 24 Unicode characters, 10 digits, 5 ligatures, 16 vowels, and 41 consonants.<br/>


a: a (vowel)<br/>
b: b<br/>
c: c<br/> 
d: d<br/> 
e: e (vowel)<br/> 
f: f<br/> 
g: g<br/> 
h: h<br/> 
i: i (vowel)<br/> 
j: j<br/> 
k: k<br/> 
l: l<br/> 
m: m<br/> 
n: n<br/> 
o: o (vowel)<br/> 
p: p<br/> 
q: q (gh)<br/> 
r: r<br/> 
s: s<br/> 
t: t<br/> 
u: u (vowel)<br/> 
v: v<br/> 
x: x<br/> 
y: y<br/> 
z: z<br/>
<br/>
A: â (ā) (vowel)<br/> 
B: bv<br/> 
C: ŝc (shc) (ligature)<br/> 
D: dv (ligature)<br/> 
E: ē (vowel)<br/> 
H: ŝh (shh) (ligature)<br/> 
I: ī (vowel)<br/> 
J: ŝj (shj) (ligature)<br/> 
M: ma (ligature)<br/> 
N: ny (alternate form)<br/> 
O: ō (vowel)<br/> 
P: ŝp (shp) (ligature)<br/> 
S: ŝ (sh)<br/> 
T: tt<br/> 
U: ū (vowel)<br/> 
V: v (alternate form)<br/> 
X: ŝx (shx) (ligature)<br/> 
Y: y (alternate form)<br/> 
Z: ž (zh)<br/> 
<br/>
0: 0 (digit, from right to left)<br/>
1: 1 (digit, from right to left)<br/>
2: 2 (digit, from right to left)<br/>
3: 3 (digit, from right to left)<br/>
4: 4 (digit, from right to left)<br/>
5: 5 (digit, from right to left)<br/>
6: 6 (digit, from right to left)<br/>
7: 7 (digit, from right to left)<br/>
8: 8 (digit, from right to left)<br/>
9: 9 (digit, from right to left)<br/>
<br/>
00C2 + Alt + x: ŝâ<br/> 
00C5 + Alt + x: Å (vowel)<br/> 
00C6 + Alt + x: aee (vowel)<br/> 
00E2 + Alt + x: ŝa<br/> 
00E3 + Alt + x: an (vowel)<br/> 
00E5 + Alt + x: å (vowel)<br/> 
00E6 + Alt + x: ae (vowel)<br/> 
00F1 + Alt + x: ngv<br/> 
0100 + Alt + x: ŝÅ<br/> 
0101 + Alt + x: ŝå<br/> 
0104 + Alt + x: ŝaen<br/> 
0105 + Alt + x: aen (vowel)<br/> 
010C + Alt + x: âc<br/> 
010D + Alt + x: ac<br/> 
010E + Alt + x: zd<br/> 
0121 + Alt + x: gg<br/> 
0124 + Alt + x: âh<br/> 
0125 + Alt + x: ah<br/> 
0134 + Alt + x: âj<br/> 
0135 + Alt + x: aj<br/> 
0144 + Alt + x: ny<br/> 
014A + Alt + x: ngy<br/> 
014B + Alt + x: ng<br/> 
0158 + Alt + x: râ<br/> 
0159 + Alt + x: ra<br/> 
015B + Alt + x: shy<br/> 
0162 + Alt + x: ŝtt<br/> 
0163 + Alt + x: ŝt<br/> 
017D + Alt + x: zv<br/> 
03B2 + Alt + x: bh<br/> 
03B4 + Alt + x: dh<br/> 
03B8 + Alt + x: th<br/> 
1E24 + Alt + x: ŝxy<br/> 
1E25 + Alt + x: xy<br/> 
1E43 + Alt + x: hm<br/> 
1E46 + Alt + x: ŝnn<br/> 
1E47 + Alt + x: nn<br/> 
1E56 + Alt + x: ap<br/> 
1E63 + Alt + x: ssh<br/> 
1E8A + Alt + x: ŝxv<br/> 
1E8B + Alt + x: xv<br/> 
1E8F + Alt + x: yy<br/> 


For example, following is how to reverse the order of English letters (change the order from left to right to right to left), and write the first verses from Šâhnâmag (The Book of Kings):

darx du nAj e dnavAdox e mAn dap<br/>
darazogen rab ratrab aSidna ni za ek<br/>
nAj e dnavAdox du mAn e dnavAdox<br/>
yAmenhAr e hed izur e dnavAdox<br/>
